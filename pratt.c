#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include <ctype.h>

enum { TOK_UNKNOWN, TOK_NUMBER, TOK_ADD, TOK_SUB, TOK_MUL, TOK_DIV, TOK_BRACEL, TOK_BRACER, TOK_LAST} token;

int number;
int offset = 0;
char str[512];
int str_size;

typedef struct AST {
    int token;
    int number;
    struct AST *left, *right;
} AST;

AST* parse(int rbp);
AST* parser_none_nud();
AST* parser_none_led(AST* left);
AST* parser_bracel_nud();
AST* parser_bracel_led(AST* left);
AST* parser_number();
AST* parser_unary_sub();
AST* parser_op(AST* left);
int consume(const char* str, int offset, int max);

struct {
    int bp;
    AST* (*nud)();
    AST* (*led)(AST*);
} tokens[TOK_LAST] = {
[TOK_UNKNOWN] = {.bp = 0, .nud = parser_none_nud, .led = parser_none_led},
[TOK_NUMBER] = {.bp = 0, .nud = parser_number, .led = parser_none_led},
[TOK_ADD] = {.bp = 1, .nud = parser_none_nud, .led = parser_op},
[TOK_SUB] = {.bp = 1, .nud = parser_unary_sub, .led = parser_op},
[TOK_MUL] = {.bp = 2, .nud = parser_none_nud, .led = parser_op},
[TOK_DIV] = {.bp = 2, .nud = parser_none_nud, .led = parser_op},
[TOK_BRACEL] = {.bp = -1, .nud = parser_bracel_nud, .led = parser_bracel_led},
[TOK_BRACER] = {.bp = -1, .nud = parser_none_nud, .led = parser_none_led},
};

AST zero = {TOK_NUMBER, 0, 0, 0};

AST* parser_none_nud() {
    fprintf(stderr, "Error nud: %d at %d", token, offset);
    exit(1);
}
AST* parser_none_led(AST* left) {
    fprintf(stderr, "Error led: %d at %d", token, offset);
    exit(1);
}

AST* parser_number() {
   AST* ast = malloc(sizeof(AST));
   ast->token = TOK_NUMBER;
   ast->number = number;
   return ast;
}

AST* parser_op(AST* left) {
   AST* ast = malloc(sizeof(AST));
   ast->token = token;
   ast->left = left;
   ast->right = parse(tokens[token].bp);
   return ast;
}

AST* parser_unary_sub() {
    AST* ast = malloc(sizeof(AST));
    ast->token = TOK_SUB;
    ast->left = &zero;
    ast->right = parse(3);
    return ast;
}
AST* parser_bracel_nud() {
    AST* ast = parse(0);
    offset = consume(str, offset, str_size);
    if (token != TOK_BRACER) {
        fprintf(stderr, "Expected closing brace at %d\n", offset);
        exit(1);
    }
    return ast;
}
AST* parser_bracel_led(AST* left) {
    return parser_none_led(left); // TODO: functions
}

AST* parse(int rbp) {
    if(offset == str_size) {
        fprintf(stderr, "Premature end of string\n");
        exit(1);
    }
    offset = consume(str, offset, str_size);
    AST* left = tokens[token].nud();

    while (offset < str_size) {
        int t = token;
        int newoffset = consume(str, offset, str_size);
        if(tokens[token].bp > rbp) {
            offset = newoffset;
            left = tokens[token].led(left);
        } else {
            token = t;
            break;
        }
    }
    return left;
}

int consume(const char* str, int offset, int max) {
    while(isspace(str[offset]) && offset < max) offset++;
    if(offset == max) return offset;
    if(isdigit(str[offset])) {
        int c = 0;
        while(isdigit(str[offset]) && offset < max) {
            c *= 10;
            c += str[offset] - '0';
            offset++;
        }
        token = TOK_NUMBER;
        number = c;
        return offset;
    }
    switch (str[offset]) {
        case '+':
            token = TOK_ADD;
            return offset + 1;
        case '-':
            token = TOK_SUB;
            return offset + 1;
        case '*':
            token = TOK_MUL;
            return offset + 1;
        case '/':
            token = TOK_DIV;
            return offset + 1;
        case '(':
            token = TOK_BRACEL;
            return offset + 1;
        case ')':
            token = TOK_BRACER;
            return offset + 1;
        default:
            token = TOK_UNKNOWN;
            return offset;
    }
}

void print_ast(AST* t, int level) {
    for(int i = 0; i < level; i++) putc('-', stdout);
    if(t->token == TOK_NUMBER) printf("Number %d\n", t->number);
    else printf("Token %d\n", t->token);
    if(t->left) print_ast(t->left, level+1);
    if(t->right) print_ast(t->right, level+1);
}


int main() {
    fgets(str, 512, stdin);
    str_size = strlen(str);
    print_ast(parse(0), 0);
}
